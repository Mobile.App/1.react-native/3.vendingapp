/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import BasicFlatList from './component/BasicFlatList';
import Video from 'react-native-video';
import Animated,{Easing, Extrapolate} from 'react-native-reanimated'
import SwitchSelector from "react-native-switch-selector";
import PropTypes from 'prop-types';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Realm = require('realm');
const {width, height} = Dimensions.get('window');
const {Value,event,block,cond,eq,set,Clock,startClock,stopClock,debug,timing,clockRunning,interpolate,extrapolate} = Animated;
const videoLink = require('./assets/video.mp4');
const bgImg = require('./assets/bg.jpg');
const cocaGif = require('./assets/coca.gif')
const logo = require('./assets/logo.png')
const qrImg = require('./assets/qr.png');
// Define your models and their properties
class Product{
  
}
const ProductSchema = {
  name: 'Product',
  primaryKey: 'uid',
  properties: {
    uid: 'string',
    name:  'string',
    price: 'int',
    type: 'string',
    imgLink: 'string',
  }
};
const options = [
  { label: "Tiền mặt", value: "1" },
  { label: "15000", value: "1.5" },
  { label: "Momo", value: "2" }
];
class PaymentSelector extends React.Component {
    constructor(props){
      super(props);      
      this.state={
        test: 1,
      }
      this.strMoney = '';
    }
    static propTypes = {
      isSelectedProduct: PropTypes.bool,
      money: PropTypes.int,
      opacity: PropTypes.float,
      eventPress: PropTypes.func,
    }

    render () {
      if(this.props.money > 0){
        this.strMoney = this.props.money.toString() + 'đ';
        optionss = [
          { label: this.strMoney, value: 0 },
        ];
        return(
          <View style={{opacity: this.props.opacity}}>
            <SwitchSelector
              options={optionss}
              initial={0}
              //onPress={value => console.log(`Call onPress with value: ${value}`)}
            />
          </View>
        )
      } 
      else if(this.props.isSelectedProduct){
        optionss = [
          { label: "Tiền mặt", value: "1" },                
          { label: "Momo", value: "2" }
        ];
        return(
          <Animated.View style={{opacity: this.props.opacity}}>
            <SwitchSelector
              options={optionss}
              initial={0}
              onPress={value => this.props.eventPress(value)}
            />
          </Animated.View>
        )
      }
      else{
        return(null)
      }      
  }
};

class LayoutSetting extends React.Component{
  constructor(props){
    super(props);
  }
  static propTypes = {
    headerFlex: PropTypes.int,
    productFlex: PropTypes.int,
    footFlex: PropTypes.int,
  }
  render () {
    const array = [
      'Flex Video',
      'Flex Product',
      'Flex Footer',
      'Product - Margin Top',
      'Product - Margin Bottom',
      'Payment Select Height',
      'Return Height',
      'Item Font Size',
      'Payment Select Font Size',
    ];
    return (
      <SafeAreaView>
        <ScrollView>
          <Text style={stylesHomeSetting.title}>LAYOUT SETTING</Text>
          {
            array.map(i=>{
              return(<View style={stylesHomeSetting.lineInput}>
                <Text style={{flex: 2}}>{i}</Text>
                <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth: 1, flex: 3}}
                />
                </View>
              )
            })
          }        
          <View style={stylesHomeSetting.button}>
            <TouchableOpacity>
              <Text style={stylesHomeSetting.buttonText}>Update</Text>
            </TouchableOpacity>
          </View>
          <View style={stylesHomeSetting.button}>
            <TouchableOpacity>
              <Text style={stylesHomeSetting.buttonText}>Return</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const stylesHomeSetting = StyleSheet.create({
  title:{
    textAlign: 'center',
    fontSize: 30,
    marginTop: 0,
    marginBottom: 10,
    padding: 10,
    backgroundColor: '#f3e0dc',
  },
  lineInput:{
    flexDirection: 'row',
    paddingLeft: 10,
    marginRight: 30,
    marginTop: 10,
  },
  button:{
    width: '100%',
    height: 50,
    borderRadius: 10,
    backgroundColor: '#84cecb',
    margin: 5,
    justifyContent:'center',
  },
  buttonText:{
    textAlign: 'center',
  },
});

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      
    </View>
  );
}
const Drawer = createDrawerNavigator();

class ButtonReturnMain extends React.Component {
  constructor(props){
    super(props);      
    this.state={
    }
  }
  static propTypes = {
    isSelectedProduct: PropTypes.bool,
    opacity: PropTypes.float,
    onPress: PropTypes.func
  }

  render () {
    if(this.props.isSelectedProduct){      
      return(
        <Animated.View style={{opacity: this.props.opacity, marginTop: 10, marginBottom: 5, borderRadius: 20, height: 40, justifyContent:'center', backgroundColor:'powderblue'}}>
          <TouchableOpacity onPress={this.props.onPress}>
            <Text style={{fontSize: 25, textAlign:'center'}}>Giao diện chính</Text>
          </TouchableOpacity>
        </Animated.View>
      )
    }
    else{
      return(null)
    }      
}
};
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 0,
      productData: [],
      productIndexImg: 1,
      testText: 'a',
      isSelectedProduct: false,
      gotoMenuForDebug: false,
      nameProductInfoDisplay: '',
      priceProductInfoDisplay: 0,
    }
    console.disableYellowBox = true;

    this.opacityMain = new Animated.Value(1);
    this.opacityGif = new Animated.Value(1);
    this.transformXProducts = [];
    this.transformYProducts = [];
    this.transformXFistProducts = new Animated.Value(-90);
    this.transformYFistProducts = new Animated.Value(0);
    this.scaleProduct = new Animated.Value(1);
    for(i = 0; i < 16; i++){
      this.transformXProducts.push(new Animated.Value(-90*(i + 1)));
    }
    for(i = 0; i < 16; i++){
      this.transformYProducts.push(new Animated.Value(0));
    }
    this.opacityPaymentSelect = new Animated.Value(0);
    this.opacityPriceText = new Animated.Value(1);
    this.opacityQr = new Animated.Value(0);

    // Variable config
    this.totalProduct = 0;
    this.totalRowProduct = 0;
    this.totalColProduct = 0;
    this.scaleProductConfig = 1.8; 

    // Variable running
    this.indexProductSelect = 0;
    this.rowProductSelect = 0;
    this.colProductSelect = 0;
    this.moneyEscrow = 0;
    this.numPressLogoForDebug = 0;
    this.isSetTimeoutResetPressLogoForDebug = false;
    
    /*
    for(i = 0; i < 8; i++){
      this.transformXProducts[i] = interpolate(this.transformXFistProducts,{
        inputRange:[-90,0],
        outputRange:[-90*(i + 1),0],
        extrapolate:Extrapolate.CLAMP
      })      
    }

    for(i = 0; i < 8; i++){
      this.transformYProducts[i] = interpolate(this.transformYFistProducts,{
        inputRange:[0,height],
        outputRange:[0,((i/3) + 1)*height],
        extrapolate:Extrapolate.CLAMP
      })
    }*/

    Realm.open({path: 'database.realm',schema: [ProductSchema],schemaVersion: 2})
      .then(realm => {
        /*
          // Create Realm objects and write to local storage
          realm.write(() => {
            const product = realm.create('Product', {
              uid:'c8687d75-f32e-4d4e-82bf-a64a31ed7061',
              name: 'Pepsi',
              price: 10000,
              type: 'Can',
              imgLink: 'YbkDkLd/c8687d75-f32e-4d4e-82bf-a64a31ed706c.png',
            });          
          });   

          realm.write(() => {
            const product = realm.create('Product', {
              uid:'89b05d26-eb08-48d0-8ab5-33278f76de42',
              name: 'Redbull',
              price: 12000,
              type: 'Can',
              imgLink: 'Lr8HPVr/redbull.png',
            });          
          }); 
        */
        
        productDb = realm.objects('Product');        
        var productDataStr = '[[' + JSON.stringify(productDb[0]);
        for(i = 1; i < productDb.length; i++){
          productDataStr = productDataStr + ',' + JSON.stringify(productDb[i]);
        }
        productDataStr = productDataStr + '],[' + JSON.stringify(productDb[0]);    
        for(i = 1; i < productDb.length; i++){
          productDataStr = productDataStr + ',' + JSON.stringify(productDb[i]);
        }
        productDataStr = productDataStr + ']]';

        if(productDb.length == 0){
          productDataStr = '[[{"uid":"c8687d75-f32e-4d4e-82bf-a64a31ed7062","name":"Revive","price":10000,"type":"Can","imgLink":"Ln88FF3/revive.png"},{"uid":"c8687d75-f32e-4d4e-82bf-a64a31ed7061","name":"Trà xanh","price":10000,"type":"Can","imgLink":"sKy7Lx4/khongdo.png"},{"uid":"89b05d26-eb08-48d0-8ab5-33278f71de42","name":"Number one","price":12000,"type":"Can","imgLink":"0hqn1d5/number-One.png"},{"uid":"89b05d26-eb08-48d0-8ab5-33278f76de42","name":"Sting","price":12000,"type":"Can","imgLink":"ZHJhsLK/sting.png"}],[{"uid":"c8687d75-f32e-4d4e-82bf-a64a31ed7062","name":"Cocacola","price":10000,"type":"Can","imgLink":"MNLWFjV/coca.png"},{"uid":"c8687d75-f32e-4d4e-82bf-a64a31ed7061","name":"Pepsi","price":10000,"type":"Can","imgLink":"YbkDkLd/c8687d75-f32e-4d4e-82bf-a64a31ed706c.png"},{"uid":"89b05d26-eb08-48d0-8ab5-33278f71de42","name":"Redbull","price":12000,"type":"Can","imgLink":"Lr8HPVr/redbull.png"},{"uid":"89b05d26-eb08-48d0-8ab5-33278f76de42","name":"Mirinda","price":12000,"type":"Can","imgLink":"9b4dqzy/mirinda-Thai.png"}]]';
        }
        // Remember to close the realm when finished.
        realm.close();
        this.setState({productData: JSON.parse(productDataStr)});
        this.totalRowProduct = Object.keys(this.state.productData).length;
        this.totalColProduct = Object.keys(this.state.productData[0]).length;
        this.totalProduct = (this.totalRowProduct - 1)*this.totalColProduct + Object.keys(this.state.productData[this.totalRowProduct - 1]).length;
        setTimeout(()=>this.initScreen(), 1000);
      })
      .catch(error => {
        console.log(error);
      });
  }
  initScreen(){
    // Clear status selected product
    this.setState({isSelectedProduct: false});
    
    this.transformXFistProducts.setValue(-90);
    this.transformYFistProducts.setValue(0);
    for(i = 0; i < 4; i++){
      this.transformYProducts[i].setValue(0);
    }
    for(i = 4; i < 8; i++){
      this.transformYProducts[i].setValue(0);
    }
    this.scaleProduct.setValue(1);
    
    // Animation timming for display product
    for(i = 0; i < 16; i++){
      this.transformXProducts[i].setValue(-90*(i + 1));
      Animated.timing(
        this.transformXProducts[i],
        {
          toValue: 0,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
  }
  componentDidMount(){
    
  }
  
  hiddenMain(row, col){
    if(this.state.isSelectedProduct) return;
    //this.state.isSelectedProduct = true;
    this.opacityPaymentSelect.setValue(0);
    this.setState({isSelectedProduct: true});

    this.rowProductSelect = row;
    this.colProductSelect = col;
    this.indexProductSelect = this.totalColProduct*row + col;
    for(i = 0; i < this.indexProductSelect; i++){
      this.transformYProducts[i].setValue(0);
      Animated.timing(
        this.transformYProducts[i],
        {
          toValue: ((i/3) + 1)*height,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
    for(i = (this.indexProductSelect + 1); i < 16; i++){
      this.transformYProducts[i].setValue(0);
      Animated.timing(
        this.transformYProducts[i],
        {
          toValue: ((i/3) + 1)*height,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
    setTimeout(()=>this.moveProductSelectCenter(), 500);
  }
  moveProductSelectCenter(){
    let rowProduct = Math.floor(this.indexProductSelect/this.totalColProduct);
    let colProduct = this.indexProductSelect%this.totalColProduct;    

    let colProductOfThisRow = 0;
    if(this.totalProduct < ((rowProduct + 1)*this.totalColProduct)){
      colProductOfThisRow = this.totalProduct%this.totalColProduct;
    }
    else{
      colProductOfThisRow = this.totalColProduct;
    }

    let scale = (this.scaleProductConfig*colProductOfThisRow)/this.totalColProduct;
    fixRow1 = 0;
    if(this.indexProductSelect < 4){
      fixRow1 = 20;
    }
    Animated.timing(
      this.transformYProducts[this.indexProductSelect],
      {   
        toValue: ((height*15/21)/this.totalRowProduct)*(((this.totalRowProduct - 1)/2) - rowProduct)/scale - fixRow1,
        duration: 200,
        easing: Easing.linear
      }
    ).start();

    Animated.timing(
      this.transformXProducts[this.indexProductSelect],
      {
        toValue: ((width/colProductOfThisRow)*((colProductOfThisRow/2) - colProduct) - ((width/8)))/scale + (width/8),
        duration: 200,
        easing: Easing.linear
      }
    ).start();
    Animated.timing(
      this.opacityPriceText,
      {
        toValue: 0,
        duration: 100,
        easing: Easing.linear
      }
    ).start();
    Animated.timing(  
      this.scaleProduct,
      {
        toValue: scale,
        duration: 500,
        easing: Easing.linear
      }
    ).start(()=>this.initPaymentSelect());
  }
  initPaymentSelect(){
    // Init product infomation
    this.state.nameProductInfoDisplay = this.state.productData[this.rowProductSelect][this.colProductSelect].name;
    this.setState({priceProductInfoDisplay: this.state.productData[this.rowProductSelect][this.colProductSelect].price});

    this.opacityPaymentSelect.setValue(0);
    Animated.timing(
      this.opacityPaymentSelect,
      {   
        toValue: 1,
        duration: 300,
        easing: Easing.linear
      }
    ).start();
  }
  hiddenProductSelected(){
    // Animated moving product selected to bottom
    Animated.timing(
      this.transformYProducts[this.indexProductSelect],
      {   
        toValue: height/2,
        duration: 400,
        easing: Easing.linear
      }
    ).start(()=>setTimeout(()=>this.initScreen(), 1));
    // Animation timming for hidden payment select
    Animated.timing(
      this.opacityPaymentSelect,
      {
        toValue: 0,
        duration: 500,
        easing: Easing.linear
      }
    ).start();
    // Animation timming for hidden qr image
    Animated.timing(
      this.opacityQr,
      {
        toValue: 0,
        duration: 500,
        easing: Easing.linear
      }
    ).start();
    
  }
  checkMultiPressLogoForDebug(){
    // Increase counter for press logo
    this.numPressLogoForDebug++;
    // Check counter press logo allow to access menu
    if(this.numPressLogoForDebug > 3){
      this.setState({gotoMenuForDebug: true});      
    }
    
    if(!this.isSetTimeoutResetPressLogoForDebug){
      this.isSetTimeoutResetPressLogoForDebug = true;
      setTimeout(()=>this.ResetPressLogoForDebug(), 5000);
    }
  }
  ResetPressLogoForDebug(){
    this.isSetTimeoutResetPressLogoForDebug = false;
    this.numPressLogoForDebug = 0;
    this.setState()
  }
  CheckChoosePaymentMethod(value){
    console.log(value);
    if(value == 2){
      Animated.timing(
        this.opacityQr,
        {
          toValue: 1,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
    else{
      Animated.timing(
        this.opacityQr,
        {
          toValue: 0,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
  }

  
  render(){
    if(!this.state.gotoMenuForDebug){
      return(
        <Animated.View style={{flex: 1,
          flexDirection:'column',
          //backgroundColor: 'powderblue',
          opacity: this.opacityMain,}}>
          <Image source={bgImg} style={styles.backgroundImage}>
            
          </Image>
          <View style={styles.videoArea}>
            <Video source={videoLink}   // Can be a URL or a local file.
              ref={(ref) => {
                this.player = ref
              }}                                      // Store reference
              onBuffer={this.onBuffer}                // Callback when remote video is buffering
              onError={this.videoError}               // Callback when video cannot be loaded
              repeat={true}
              resizeMode={"cover"}
              style={styles.headerVideo} />
          </View>
          <View style={styles.productTable}>
            {
              this.state.productData.map((i, indexRow) => {
              elementOnRow = Object.keys(i).length;
              return(
                <Animated.View style={{flex:1, flexDirection:'row'}}>                  
                  {i.map((j, indexCol) => {
                    //const imgObj = require(strImg.toString());
                    return(
                      <TouchableOpacity style={{flex:1, flexDirection:'row'}} onPress={()=>this.hiddenMain(indexRow,indexCol)}>
                        <Animated.View style={{flex:1,
                                              flexDirection:'column',
                                              //backgroundColor: 'powderblue',
                                              textAlign:'center',
                                              margin: 10, transform:[{scale: this.scaleProduct, translateX: this.transformXProducts[this.totalColProduct*indexRow + indexCol], translateY: this.transformYProducts[this.totalColProduct*indexRow + indexCol]}]}}>
                          
                          
                          <View style={styles.imageView}>
                            <AutoHeightImage width={width/(elementOnRow*1.5)} style={{alignContent:'center'}} source={{uri: 'https://i.ibb.co/' + j.imgLink + '.png'}}/>
                          </View>
                          <Animated.View style={{alignItems: 'center',
                                        fontSize: 10,
                                        width: '100%',
                                        backgroundColor: '#e27d60',
                                        borderRadius: 10,
                                        opacity: this.opacityPriceText,
                                      }}
                          >
                            <Text style={styles.priceText}>10000đ</Text>
                          </Animated.View>
                        </Animated.View>
                      </TouchableOpacity>
                    );
                  })}
                  
                </Animated.View>
              );
            })}
            <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
              <PaymentSelector opacity={this.opacityPaymentSelect} isSelectedProduct={this.state.isSelectedProduct} money={0} eventPress={(x)=>this.CheckChoosePaymentMethod(x)}></PaymentSelector>
              <ButtonReturnMain opacity={this.opacityPaymentSelect} isSelectedProduct={this.state.isSelectedProduct} onPress={()=>this.hiddenProductSelected()}></ButtonReturnMain>
            </View>
            <View style={{
              position: 'absolute',
              left: 45,
              top: 70,
            }}>
              <Animated.View style={{
                opacity: this.opacityPaymentSelect,
                alignItems: 'center',
                textAlign: 'center',
              }}
              >
                <Text style={{
                  fontSize: 30,
                  color: 'red',
                  fontWeight: 'bold',
                }}>{this.state.nameProductInfoDisplay}</Text>
                <Text style={{
                  fontSize: 20,
                }}>{this.state.priceProductInfoDisplay}</Text>
              </Animated.View>
              <Animated.View style={{
                opacity: this.opacityQr,
                marginTop: 40,
                alignItems: 'center',
              }}>
                <AutoHeightImage width={100} style={{alignContent:'center'}} source={qrImg}/>
              </Animated.View>
              
            </View>
          </View>
          
          
          
          <View style={styles.footer}>
            <TouchableOpacity onPress={()=>this.checkMultiPressLogoForDebug()}>
              <Image style={{flex: 1, resizeMode: 'contain'}} source={logo}/>
            </TouchableOpacity>
          </View>
        </Animated.View>
      )
    }
    else{      
      return(
        <NavigationContainer>
          <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Layout Setting" component={LayoutSetting} />
            <Drawer.Screen name="Notifications" component={NotificationsScreen} />
          </Drawer.Navigator>
        </NavigationContainer>
      )
    }
  }
}


const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    opacity: 1,
  },
  body: {
    flex: 1,
    flexDirection:'column',
    //backgroundColor: 'powderblue',
    opacity: 1,
  },
  videoArea:{
    flex:5,
    flexDirection:'row',
    //backgroundColor:'red',
    justifyContent: 'center',
  },
    headerVideo: {
      flex: 1,    
    },
  productTable:{
    flex:15,
    flexDirection:'column',
    marginTop:10,
    justifyContent: 'space-around',
  },
    productRow:{
      flex:1,
      flexDirection:'column',
    },
      productItem:{
        flex:1,
        flexDirection:'row',
        //backgroundColor: 'powderblue',
        margin: 10,
        flexWrap: 'wrap'
      },
        priceItem:{
          alignItems: 'center',
          fontSize: 10,
          width: '100%',
          backgroundColor: '#e27d60',
          borderRadius: 10,          
        },
        priceText:{
          color: 'white',
        },
        imageView:{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        },
        imageItem:{
          
        },
        
  moneyEscrow:{
    flex:2,
    alignItems:'center',
  },
  footer:{
    flex:1,
    opacity: 1,
    backgroundColor: '#888888',
    alignItems:'center',
    paddingTop: 2,
  },
  
});

export default App;
