import React, { Component } from 'react';
import { View, Text, AppRegistry, FlatList, StyleSheet, Button } from 'react-native';
import foodListData from '../data/foodListData';

class FlatListItem extends Component{
  render(){
    return(
      <View style={{
        flex: 1,
        backgroundColor: 'powderblue',
        margin: 10,
        paddingLeft: 5,
        paddingRight: 5,
        justifyContent: 'center'
      }}>
        <Text style={{flex: 1, textAlign: 'center'}}>{this.props.item.name}</Text>
        <Text style={{flex: 1, textAlign: 'center'}}>{this.props.item.foodDescription}</Text>
      </View>
    );
  }
}
class BasicFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      init:false,
      offsetValue: 0,
      direct: 0,
    };
  }
  running = ()=>{
    this._flatList.scrollToOffset({animated: true, offset: this.state.offsetValue});
    if(this.state.direct == 0){
      this.state.offsetValue += 1;
      if(this.state.offsetValue == 400){
        this.state.direct = 1;
      }
    }
    else{
      this.state.offsetValue -= 1;
      if(this.state.offsetValue == 0){
        this.state.direct = 0;
      }
    }
    
    //console.log(this.state.offsetValue);
  }
  componentDidMount(){
    if(this.state.init==false){
      this.state.init = true;
      setInterval(this.running, 10);
    }
  }

  render() {
    return (
      <View style={{flex:1, flexDirection:'column', marginTop:22}}>
        <Button onPress={this.running}
            title="Tap to scrollToIndex"
            color="darkblue">

        </Button>
        <FlatList
          ref={(ref) => { this._flatList = ref; }}
          data={foodListData}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{flex: 1}}
          
          renderItem={({item,index})=>{
            return(
              <FlatListItem item={item} index={index}>
            
              </FlatListItem>
            );
          }}>          
        </FlatList>
      </View>
    );
  }
}

export default BasicFlatList;
